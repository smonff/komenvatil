const buildApiUrl = (url, v) => {
  const api_conf = `${process.env.REACT_APP_PROTOCOL}://` +
        `${process.env.REACT_APP_HOST}` +
        getPort()
        +
        `/${url}/${v}`;
  console.log(api_conf);
  return api_conf;
};

const getPort = () => {
  console.log(process.env.REACT_APP_PORT);
  return process.env.REACT_APP_PORT !== '' ?
    `:${process.env.REACT_APP_PORT}` : '';
};

export { buildApiUrl }
