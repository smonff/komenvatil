import React, { Component } from 'react';
// import logo from './logo.svg';
import './App.scss';
import axios from 'axios';
import { buildApiUrl } from './api/fetch.js';
import RerSlider from './components/RerSlider';
import Results from './components/Results';
import NavBar from './components/Navbar';
import { Jumbotron } from 'reactstrap';
import NotificationGenerator from './components/NotificationGenerator';
import moment from 'moment';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { fab } from '@fortawesome/free-brands-svg-icons';
import { library } from '@fortawesome/fontawesome-svg-core';
library.add(fab);

class App extends Component {
  constructor(props) {
    super(props);
    this.results();
    this.state = {
      average: '',
      mood: '',
      results: '',
      vote: '',
    };
  }

  // toggleNavbarModal() {
  //   this.setState({
  //     openIt: !this.openIt,
  //   });
  // }

  render() {
    const color = this.getCurrentStatus(this.state.average);
    return (
      <div className="App container-fluid">
        <NavBar toggle={this.state.openIt} results={this.state.results} color={color} />
        <main>
          <Jumbotron className="row justify-content-center text-muted">
            <p className="lead">Si vous êtes dans une station RER B ou à bord
              d'un train, donnez votre avis pour partager l'humeur du moment!</p>
          </Jumbotron>
          <div id="content" className="row justify-content-center">
            <RerSlider results={this.results} vote={this.vote} />
            <Results
              average={this.state.average}
              results={this.state.results}
              mood={this.state.mood}
            />
          </div>
          <NotificationGenerator vote={this.state.vote} type={this.state.vote} />
        </main>
        <footer className="row text-muted">
          <div className="container-fluid p-3 p-md-5">
            <ul>
              <li>
                <a href='#' /* onClick={(e) => this.toggleNavbarModal} */>
                  À propos
                </a>
              </li>
              <li>
                <a href='mailto:conducteur@comment-va-t-il.net'>Contact</a>
              </li>
              <li>
                <a href='https://gitlab.com/smonff/komenvatil/'>
                  Open Source  <FontAwesomeIcon icon={['fab', 'gitlab']} />
                </a>
              </li>
              <li>
                <a href='https://twitter.com/komenvatil_rerb/'>
                  Twitter <FontAwesomeIcon icon={['fab', 'twitter']} />
                </a>
              </li>
            </ul>
            <p>
              Copyright 2018-{moment().year()} Balik Network. Pas lié à la RATP ou la SNCF.
              Merci d'envoyer vos commentaires à <a
                                                   href='mailto:bug@comment-va-t-il.net'
                                                 >bug@comment-va-t-il.net</a>.
            </p>
            <p>
              Version { this.state.version } - B<sup>eta</sup>.
              Code en <a
                        href='https://gitlab.com/smonff/komenvatil/blob/master/LICENSE'>
                        Artistic Licence 2.0
                      </a>
            </p>
            <p>Made with <a href='//mojolicious.org'><img alt='Mojolicious' src='/mojo_logo.png' /></a></p>
          </div>
      </footer>
      </div>
    );
  }

  vote = (score) => {
    console.log(`Vote ${score}`);
    this.setState({
      vote: score,
    });
    axios.post(buildApiUrl('api', 'vote'), {
      score: score
    })
      .then(response => {
        console.log(response.data);
        this.results();
      })
      .catch(error => {
        console.log(error);
        console.warn('Check CORS');
      });
  }

  results = () => {
    axios.get(buildApiUrl('api', 'votes'))
      .then(response => {
        this.setState({
          results: response.data.votes.length,
          average: response.data.average,
          mood: response.data.mood,
          version: response.data.version,
        });
      })
      .catch(error => {
        console.log(error);
        console.warn('Check CORS');
      });
  }

  getCurrentStatus = (average) => {
    if ( average === '' ) return 'void';
    if (average < 33) {
      return 'danger';
    } else if (average > 33 && average < 66) {
      return 'warning';
    } else { return 'success'; }
  }
}

export default App;
