const C = {
  MOOD_RANGES: {
    SAD_MAX: 33,
    MEH_MAX: 66,
  }
};

export default C;
