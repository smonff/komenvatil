import React, { Component } from 'react';
import 'rc-slider/assets/index.css';
import 'rc-tooltip/assets/bootstrap.css';
import Slider from 'rc-slider';
import Tooltip from 'rc-tooltip';
import { Button } from 'reactstrap';
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faSmile,  faMeh, faFrown
} from '@fortawesome/free-regular-svg-icons';
library.add( faSmile, faMeh, faFrown );

const createSliderWithTooltip = Slider.createSliderWithTooltip;
const Range = createSliderWithTooltip(Slider.Range);
const Handle = Slider.Handle;

class RerSlider extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedSlider: 50,
    };
  }

  onSliderChange = (value) => {
    this.setState({
      selectedSlider: value
    });
  }

  increment = () => {
    if ( this.state.selectedSlider < 100 ) {
      this.setState({
        selectedSlider: this.state.selectedSlider + 50
      });
    }
  }

  decrement = () => {
    if ( this.state.selectedSlider > 0 ) {
      this.setState({
        selectedSlider: this.state.selectedSlider - 50
      });
    }
  }


  render() {
    return (
      <div className="col-sm-6 over slider">
        <div className="row justify-content-center">
          <div className="col-12 col-sm-12">
            <div className="row justify-content-center separator-bottom">
              <FontAwesomeIcon
                color={this.getFrownColor(this.state.selectedSlider)}
                className="col-2"
                icon={['far', 'frown']}
                size="2x"
                onClick={(e) => this.decrement()}
              />
              <div className="col-8 col-sm-8 col-lg-6">
                <Slider
                  step={50}
                  defaultValue={this.state.selectedSlider}
                  value={this.state.selectedSlider}
                  onAfterChange={log}
                  onChange={(e) => this.onSliderChange(e)}
                  dots={true}
                  trackStyle={{ backgroundColor: '#007bff'}}
                  handleStyle={{ borderColor: '#007bff'}}
                  dotStyle={{ borderColor: '#7ba3dc'}}
                  activeDotStyle={{ borderColor: '#7ba3dc'}}
                  handle={handle}
                />
              </div>
              <FontAwesomeIcon
                color={this.getSmileColor(this.state.selectedSlider)}
                className="col-2"
                icon={['far', 'smile']}
                size="2x"
                onClick={(e) => this.increment()} />
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-12">
            <Button
              onClick={e => this.props.vote(this.state.selectedSlider)}
              style={buttonStyle}
              color={this.getButtonColor(this.state.selectedSlider)}
              size="lg"
            >
              Témoigner
            </Button>
          </div>
        </div>
      </div>
    );
  }

  getFrownColor = (sliderValue) => {
    switch(sliderValue) {
    case 0:
      return 'rgba(0, 0, 0, .45)';
      break;
    default:
      return 'rgba(0, 0, 0, .25)';
    }
  }

  getSmileColor = (sliderValue) => {
    switch(sliderValue) {
    case 100:
      return 'rgba(0, 0, 0, .45)';
      break;
    default:
      return 'rgba(0, 0, 0, .25)';
    }
  }


  getButtonColor = (sliderValue) => {
    switch(sliderValue) {
    case 0:
      return 'danger';
      break;
    case 50:
      return 'warning';
      break;
    case 100:
      return 'success';
      break;
    }
  }
}

const handle = (props) => {
  const { value, dragging, index, ...restProps } = props;
  const marks = { 0: "Mécontent·e", 50: "Neutre", 100: "Satisfait·e"};
  return (
    <Tooltip
      prefixCls="rc-slider-tooltip"
      overlay={marks[value]}
      visible={dragging}
      placement="top"
      key={index}
    >
      <Handle value={value} {...restProps} />
    </Tooltip>
  );
};

const log = (value) => console.log(value);
const buttonStyle = {
  fontSize: '150%'
};

export default RerSlider;
