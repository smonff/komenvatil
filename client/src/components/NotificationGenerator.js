import React from 'react';
import { AlertList } from 'react-bs-notifier';

class NotificationGenerator extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      position: "bottom-right",
      alerts: [],
      timeout: 3000,
    };
  }

  componentDidUpdate(prevProps) {
    console.log(this.props.mood);
    console.log(prevProps.vote);
    if (this.props.vote !== prevProps.vote) {
      return this.generate(this.props.type, this.props.vote);
    }
  }

  generate(type, vote) {
    const message = this.buildMessage(vote);
    const newAlert ={
      id: (new Date()).getTime(),
      type: this.buildType(this.props.type),
      headline: 'Merci du témoignage',
      message: message
    };

    this.setState({
      alerts: [...this.state.alerts, newAlert]
    });
  }

  onAlertDismissed(alert) {
    const alerts = this.state.alerts;

    // find the index of the alert that was dismissed
    const idx = alerts.indexOf(alert);

    if (idx >= 0) {
      this.setState({
        // remove the alert from the array
        alerts: [...alerts.slice(0, idx), ...alerts.slice(idx + 1)]
      });
    }
  }

  // TODO => ICONS
  // By handle
  // Fa exclamation
  // Fa warning
  // Fa check

  buildMessage = (vote) => {
    switch (vote) {
    case 0:
      return 'Vous êtes dégouté·e et vous n\'en peux plus';
    case 50:
      return 'Vous en avez marre mais c\'est encore supportable';
    case 100:
      return 'La situation est normale et vous avez même trouvé une place assise';
    }
  }

  buildType = (mood) => {
    switch (mood) {
    case 0:
      return 'danger';
    case 50:
      return 'warning';
    case 100:
      return 'success';
    }
  }

  render() {
    return (
      <AlertList
        position={this.state.position}
        alerts={this.state.alerts}
        timeout={this.state.timeout}
        dismissTitle="Begone!"
        onDismiss={this.onAlertDismissed.bind(this)}
      />
    );
  }
}

export default NotificationGenerator;
