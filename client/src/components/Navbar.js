import React, { Component } from 'react';
import {
  Button, Collapse, Modal, ModalHeader, ModalBody, ModalFooter, Navbar,
  NavbarToggler, NavbarBrand, Nav, NavItem
} from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faQuestion, faSubway, faTrafficLight } from '@fortawesome/free-solid-svg-icons';
import { library } from '@fortawesome/fontawesome-svg-core';
library.add(faQuestion, faSubway, faTrafficLight);

class NavBar extends Component {
  constructor(props) {
    super(props);

    this.toggleMenu = this.toggleMenu.bind(this);
    this.toggleModal =this.toggleModal.bind(this);
    this.state = {
      isMenuOpen: false,
      isModalOpen: false,
    };
  }

  toggleModal() {
    this.setState({
      isModalOpen: !this.state.isModalOpen,
    });
  }

  toggleMenu() {
    this.setState({
      isMenuOpen: !this.state.isMenuOpen
    });
  }

  render() {
    return (
      <div>
        <Navbar className="row" color="dark" dark expand="md">
          <NavbarBrand href="/">
            <h1>
              <FontAwesomeIcon icon="subway" />
              Comment va-t-il RER <span className="b">B</span>
              <FontAwesomeIcon icon="question" />
            </h1>
          </NavbarBrand>
          <NavbarToggler onClick={this.toggleMenu} />
          <Collapse isOpen={this.state.isMenuOpen} navbar>
            <Nav className="ml-auto" navbar>
              <NavItem>
                <Button onClick={this.toggleModal}>À propos</Button>
                <Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
                  <ModalHeader toggle={this.toggleModal}>À propos</ModalHeader>
                  <ModalBody>
                    <a href="/">rerb.comment-va-t-il.net</a> est une application
                    web permettant de récupérer des informations en temps réel
                    sur l'état de fonctionnement global du RER B, en se basant
                    sur les votes des utilisateurices du réseau. Les avis sont
                    effacés après 15 minutes.
                  </ModalBody>
                  <ModalFooter>
                    <a className="btn btn-info" onClick={this.toggleModal} href="mailto:info@comment-va-t-il.net">
                      Poser une question
                    </a>
                    {' '}
                    <Button color="success" onClick={this.toggleModal}>
                      Fermer
                    </Button>
                  </ModalFooter>
                </Modal>
              </NavItem>
            </Nav>
          </Collapse>
          {/* <Nav> */}
          {/*   <NavItem className="status"> */}
          {/*     { */}
          {/*       this.props.results === 0 ? '' : */}
          {/*         <FontAwesomeIcon */}
          {/*           icon="traffic-light" */}
          {/*           className={this.props.color} */}
          {/*           size='2x' /> */}
          {/*     } */}
          {/*   </NavItem> */}
          {/* </Nav> */}
        </Navbar>
      </div>
    );
  }
}

export default NavBar;
