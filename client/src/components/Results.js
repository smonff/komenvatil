import React, { Component } from 'react';
import C from '../conf';
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faSquare, faCircle, faCheck, faSkullCrossbones,
  faBan, faCloudShowersHeavy, faInfoCircle, faQuestionCircle
} from '@fortawesome/free-solid-svg-icons';
import { Alert, Badge, UncontrolledTooltip } from 'reactstrap';
library.add(
  faSquare, faCircle, faCheck, faSkullCrossbones,
  faBan, faCloudShowersHeavy, faInfoCircle,  faQuestionCircle
);

class Results extends Component {
  render() {
    return (
      <div className="col-sm-6 under">
        <div className="row results-train separator-bottom">
          <div className="col-12" style={{}} id='mood-icon'>
            {
              this.props.results === 0 ?
                this.emptyResults() :
                this.getIcon(this.props.average, this.props.results)
            }
          </div>
        </div>
        <div className="row results-mood">
          <div className="col-12">
            <h1 className="d-inline">
              <Badge color="danger" pill>{this.props.mood.sad}</Badge>
            </h1>
            <h1 className="d-inline">
              <Badge color="warning" pill>{this.props.mood.meh}</Badge>
            </h1>
            <h1 className="d-inline">
              <Badge color="success" pill>{this.props.mood.happy}</Badge>
            </h1>
          </div>
        </div>
      </div>
    );
  }

  getIcon = (average, results) => {
    let icon = '';
    if ( average === '' ) return icon;
    if (average < C.MOOD_RANGES.SAD_MAX) {
      icon = <span className="fa-layers fa-fw fa-10x">
               <FontAwesomeIcon icon="square" className="danger" />
               <FontAwesomeIcon icon="skull-crossbones" inverse transform="shrink-6" color="#dc3545"/>
             </span>;
    } else if (average >= C.MOOD_RANGES.SAD_MAX && average < C.MOOD_RANGES.MEH_MAX) {
      icon = <FontAwesomeIcon icon="cloud-showers-heavy" className="warning" size="10x" />;
    } else if (average >= C.MOOD_RANGES.MEH_MAX) {
      icon = <span className="fa-layers fa-fw fa-10x">
               <FontAwesomeIcon icon="circle" className="success" />
               <FontAwesomeIcon icon="check" inverse transform="shrink-6"/>
             </span>;
    }

    return <React.Fragment>
             {icon}
             {this.getTooltip(average, results)}
           </React.Fragment>;
  }

  getTooltip = (average, results) => {
    return <UncontrolledTooltip placement='top' target='mood-icon'>
             Note moyenne de <strong>{average} sur 100</strong> pour {results} votes
           </UncontrolledTooltip>;
  }

  emptyResults = () => {
    return <React.Fragment>
             <Alert color='info' id="question-icon">
               <span>Personne n'a encore voté.</span>
               <FontAwesomeIcon icon="question-circle" className="float-right" />
               <UncontrolledTooltip
                 placement='top' target='question-icon' autohide={false}>
                 <FontAwesomeIcon icon="info-circle" />
                 Les avis expirent automatiquement après un quart d'heure.
               </UncontrolledTooltip>
             </Alert>
           </React.Fragment>;
  }

}

export default Results;
