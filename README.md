# NAME

Komenvatil - A real-time opinion collecting application

# DESCRIPTION

Komenvatil makes possible to collect opinions about a collective situation and
build display simple reports. It provides and API and a friendly front-end.

# INSTALLATION

Setting up the minimal environment:

    $ apt install cpanminus
    $ cpanm App::Perlbrew
    $ perlbrew init

Installing Perl separately from system's Perl:

    $ perlbrew install perl-5.28.1
    $ perlbrew alias create perl-5.28.1 komenvatil
    $ perlbrew use komenvatil

Re-installing Cpanminus locally:

    $ perlbrew install-cpanm

Installing Carton:

    $ cpanm Carton

Setting-up the app:

    $ git clone https://gitlab.com/smonff/komenvatil
    $ cd komenvatil/server
    $ carton install

Starting the app in production

    $ make run-prod

# CONFIGURATION

# PLUGINS

## Minion

## Cron

## Documentation

## Yancy

# ROUTES

The routes under `/api` returns JSON ready to be consummed by the front-end.

## Home

    GET /

Index with some useful infos. Could be protected so we could link Minion, Yancy
and other helper super secret routes.

## Cors headers

All the routes under this one should be prefixed by `/api`.

    GET /api/votes
    GET /api/votes/expirated

Allow CORS origin \* for the API. All the above functions will have the
"access-control-allow-origin: \*" response header available, what allows requests
of the API from any client.

## Retrieving the application infos

    GET /api

Display the application version for now.

## Getting the votes

    GET /api/votes

Retrieve all actives votes in a structured JSON array and some calculated
informations to be used on the front-end.

    {
      "average": 50,
      "mood": {
        "happy": 1,
        "meh": 0,
        "sad": 1
      },
      "version": "0.55",
      "votes": [
        {
          "date": "2019-01-01T11:53:23",
          "vote":100
        },
        {
          "date": "2019-01-01T11:53:25",
          "vote": 0
        }
      ]
    }

## Voting

    POST /api/vote
    OPTIONS /api/vote

Post the actual vote by retrieving it from a JSON body POST request containing a
`score` parameter.

The same `OPTIONS` route is also required to allow Axios to post the
vote. Makes the `/api/vote` post possible.

## Testing votes post using a GET request

This is a very handy route that accept a vote value and an optional date thanks
to Mojolicious wilcard placeholders. It makes possible to pass an optional date
(yyyy-mm-dd format is required) for testing the expiration.

    GET /api/vote/*values

Can accept either `GET /api/vote/:value` or `GET /api/vote/:value/:date`.

For debuging purposes, not the actual voting sub. See `/api/vote`.

## Retrieving expirated votes

    GET /api/vote/expirated

Those will be passed to Minion.

## Creating a hashed password for a user

    GET /create/user/:name/:pass

Copy paste the result from the logs to the database for creating users. If you
put the value in JSON, some problems could happend with escaped '/'

# HELPERS

Most of them are helpers that build data used by the routes. They should be
turned to real Mojolicious helpers.

## get\_score()

Calculate the current score, the mean of all votes, the current _global mood_.

## get\_expirated\_votes()

Select current active votes in the database and check which one are elligible
for the expiration.

## get\_mood()

Calculate the mean moods for each _sad_, _meh_ or _happy_ mood.

## Add expirate tasks to Minion

Takes an array of expirated votes as a parameter and send them to `expirate()`
when Minion think it will be time.

## expirate()

Helps Minion to get rid if expirated votes.

## splash()

An ugly splash ASCII screen

# Acknowledgments

- Ezgi

    The whole thing is based on her idea and she contributed to all the features.

- Smonff

    Development

- Le Nid des Poussins

    Tests
