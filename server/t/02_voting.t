use Test::More;
use Test::Mojo;
use Mojo::File 'path';

my $script = path(__FILE__)->dirname->sibling('rerb');
my $t = Test::Mojo->new($script);

my @votes = (0, 50, 100);
foreach (@votes) {
    my $vote = $votes[ rand @votes];
    $t->get_ok("/api/vote/$vote")
            ->status_is(200)
            ->json_has('/value')
            ->json_is('/value' => $vote);
    $t->post_ok("/api/vote" => json => { score => $vote })
            ->status_is(200)
            ->json_has('/value')
            ->json_is('/value' => $vote);
}

done_testing();
