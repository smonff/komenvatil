use Test::More;
use DateTime;
use Test::Mojo;
use Mojo::File 'path';
use Data::Printer;

my $script = path(__FILE__)->dirname->sibling('rerb');
my $t = Test::Mojo->new($script);

# Retrieve all votes and extract only those that are not older than one
# week. Then test that the array of votes
# are different

my $time = DateTime->new(
    year      => 2018,
    month     => 12,
    day       => 3,
    hour      => 9,
    time_zone => 'Europe/Paris',
);

my $substracted_t = $time->subtract(weeks => 1);
is $substracted_t->month, 11, "Managed to subtract one week";

$t->get_ok('/api/votes/expirated')
        ->status_is(200)
        ->json_has('/expirated')
        ->json_has('/count')
        ->json_like('/count' => qr/\d/);

# If there are no votes to expire, we don't test the corresponding route.
SKIP: {
    skip unless $t->tx->res->json('/api/votes/expirated') > 0;
    $t->get_ok('/api/votes/expirated')
            ->json_like('/expirated/0' => qr/\d/);
}
# Forcing the possibility of posting expirated votes for testing purposes
$substracted_t = $time->subtract(minutes => 15);
$short_substracted_t = $substracted_t =~ s/T(\d\d):(\d\d):(\d\d)//g;

$t->get_ok('/api/vote/50/' . $substracted_t)
        ->status_is(200)
        ->json_has('/date')
        ->json_has('/value')
        ->json_like('/value' => qr/\d/)
        ->json_like(
            '/date' => qr/(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2})/);

$t->get_ok('/api/votes/expirated')
        ->json_like('/expirated/0' => qr/\d/)
        ->json_has('/count' => qr/\d/)
        ->json_like('/expirated/0' => qr/\d/, 'We have one vote to expirate');

done_testing();
