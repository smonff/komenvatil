use Test::More;
use DateTime;
use Test::Mojo;
use Mojo::File 'path';
use Data::Printer;

my $script = path(__FILE__)->dirname->sibling('rerb');
my $t = Test::Mojo->new($script);

my @mock_results = (
    {date => "Thu Dec 13 19:35:54 2018", vote => 100},
    {date => "Thu Dec 13 19:36:23 2018", vote => 50},
    {date => "Thu Dec 13 19:40:00 2018", vote => 100},
    {date => "Thu Dec 13 19:40:13 2018", vote => 0},
    {date => "Thu Dec 13 19:41:21 2018", vote => 50},
    {date => "Thu Dec 13 19:41:27 2018", vote => 100},
    {date => "Fri Dec 14 09:42:32 2018", vote => 0},
    {date => "Fri Dec 14 09:43:04 2018", vote => 100},
    {date => "Fri Dec 14 09:46:55 2018", vote => 100},
    {date => "Fri Dec 14 09:53:21 2018", vote => 100},
);

p @results;
# Retrieve all votes and extract only those that are not older than one
# week. Then test that the array of votes
# are different

my $time = DateTime->new(
    year      => 2018,
    month     => 12,
    day       => 3,
    hour      => 9,
    time_zone => 'local',
);

my $substracted_t = $time->subtract(weeks => 1);
is $substracted_t->month, 11, "Managed to subtract one week";

$t->get_ok('/api/votes/expirated')
        ->status_is(200)
        ->json_has('/expirated')
        ->json_has('/count')
        ->json_like('/count' => qr/\d/);

# If there are no votes to expire, we don't test the corresponding route.
SKIP: {
    skip unless $t->tx->res->json('/api/votes/expirated') > 0;
    $t->get_ok('/api/votes/expirated')
            ->json_like('/expirated/0' => qr/\d/);
}

done_testing();
