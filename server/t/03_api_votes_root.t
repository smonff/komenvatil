use Test::More;
use Mojo::Log;
use Test::Mojo;
use Mojo::File 'path';

my $script = path(__FILE__)->dirname->sibling('rerb');
my $t = Test::Mojo->new($script);

$t->get_ok('/api/votes')
        ->status_is(200)
        ->json_has('/average')
        ->json_has('/mood')
        ->json_has('/mood/happy')
        ->json_has('/mood/meh')
        ->json_has('/mood/sad')
        ->json_has('/votes')
        ->json_has('/votes/0/date')
        ->json_has('/votes/0/vote')
        #        ->json_like('/expirated/1' => qr/\d/)
        ;


done_testing();
