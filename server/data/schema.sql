--
-- Fichier généré par SQLiteStudio v3.2.1 sur sam. nov. 24 22:23:43 2018
--
-- Encodage texte utilisé : UTF-8
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table : votes
CREATE TABLE votes (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, mood INT NOT NULL, date DATETIME NOT NULL, active BOOLEAN DEFAULT (1) NOT NULL);

COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
