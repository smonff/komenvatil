--
-- Fichier généré par SQLiteStudio v3.2.1 sur dim. déc. 30 21:22:05 2018
--
-- Encodage texte utilisé : UTF-8
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table : users
CREATE TABLE users (id INTEGER PRIMARY KEY UNIQUE NOT NULL, username STRING UNIQUE NOT NULL, password STRING NOT NULL);

COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
